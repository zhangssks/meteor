/****************************************************
 * Description: DAO for t_sys_xfile
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-04 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.xfile.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sys.xfile.entity.XfileEntity;

public interface XfileDao  extends XjjDAO<XfileEntity> {

}

