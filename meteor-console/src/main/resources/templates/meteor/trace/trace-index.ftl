<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <input type="hidden" id="trace-className">
        <input type="hidden" id="trace-method">
        <@querygroup title='监控时长(秒)'>
            <input type="text" id="trace-time" value="10" class="form-control input-sm" placeholder="最大30秒"
                   check-type='required number'>
        </@querygroup>
        <@button type="info" onclick="sendTraceCmd()">重试</@button>
    </@query>

</@content>

<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="trace-console-card">
            <div id="trace-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var trace_ws = null;

    initTraceConsole();

    function initTraceConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        trace_ws = initArthasConsole("trace-console", path, "meteor_trace",true);

        // 设置 ctrl+c 命令 解决数据量大的问题
    }

    function sendTraceCmd(className, method) {
        if (className != null) {
            $("#trace-className").val(className);
        }
        if (method != null) {
            $("#trace-method").val(method);
        }

        var className_cmd = $("#trace-className").val();
        var method_cmd = $("#trace-method").val();

        var cmd = "trace -j  " + className_cmd + " " + method_cmd + "";
        trace_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
        //防止数据量过大，影响服务器
        var time = $("#trace-time").val();
        if (time > 30) {
            time = 30;
        }
        setTimeout(function () {
            trace_ws.send(JSON.stringify({action: 'read', data: ""}));
        }, time * 1000);
    }


</script>
