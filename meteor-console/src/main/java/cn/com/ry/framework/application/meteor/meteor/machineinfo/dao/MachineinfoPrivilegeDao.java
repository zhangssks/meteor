/****************************************************
 * Description: DAO for t_meteor_machineinfo_privilege
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-03 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.machineinfo.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoPrivilegeEntity;
import org.apache.ibatis.annotations.Param;


public interface MachineinfoPrivilegeDao extends XjjDAO<MachineinfoPrivilegeEntity> {
    void deleteByMachineId(@Param("machineId") Integer machineId);
}

