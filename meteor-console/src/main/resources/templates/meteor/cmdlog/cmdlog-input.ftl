<#--
/****************************************************
 * Description: t_meteor_cmdlog的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@input url="${base}/meteor/cmdlog/save" id=tabId>
   <input type="hidden" name="id" value="${cmdlog.id}"/>
    <@formgroup title='服务器地址'>
    <input type="text" name="hostname" value="${cmdlog.hostname}" check-type="required">
    </@formgroup>
    <@formgroup title='应用名称'>
    <input type="text" name="appname" value="${cmdlog.appname}" check-type="required">
    </@formgroup>
    <@formgroup title='命令'>
    <input type="text" name="cmd" value="${cmdlog.cmd}" check-type="required">
    </@formgroup>
    <@formgroup title='操作人员ID'>
    <input type="text" name="createPersonId" value="<#if (cmdlog.createPersonId)?? || cmdlog.createPersonId==''>${session_manager_info_key.userId}<#else>${cmdlog.createPersonId}</#if>" check-type="required" readonly="readonly">
    </@formgroup>
    <@formgroup title='操作人员名称'>
    <input type="text" name="createPersonName" value="<#if (cmdlog.createPersonName)?? || cmdlog.createPersonName==''>${session_manager_info_key.userName}<#else>${cmdlog.createPersonName}</#if>" check-type="required" readonly="readonly">
    </@formgroup>
    <@formgroup title='创建时间'>
	<@datetime name="createTime" dateValue=cmdlog.createTime required="required" default=true/>
    </@formgroup>
</@input>
