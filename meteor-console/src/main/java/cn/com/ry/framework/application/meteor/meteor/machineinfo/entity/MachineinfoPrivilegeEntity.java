/****************************************************
 * Description: Entity for t_meteor_machineinfo_privilege
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-03 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.meteor.machineinfo.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MachineinfoPrivilegeEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public MachineinfoPrivilegeEntity(){}
    private Integer machineId;//服务器ID
    private Integer userId;//用户ID
@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    /**
     * 返回服务器ID
     * @return 服务器ID
     */
    public Integer getMachineId() {
        return machineId;
    }

    /**
     * 设置服务器ID
     * @param machineId 服务器ID
     */
    public void setMachineId(Integer machineId) {
        this.machineId = machineId;
    }

    /**
     * 返回用户ID
     * @return 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoPrivilegeEntity").append("ID="+this.getId()).toString();
    }
}

