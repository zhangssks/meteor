package cn.com.ry.framework.application.meteor.meteor.javacode.entity;

public class JavacodeEntity {
    private String codeName;
    private String code;

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
