package cn.com.ry.framework.application.meteor.sys.code.service;

import cn.com.ry.framework.application.meteor.sys.code.entity.ColumnInfo;
import cn.com.ry.framework.application.meteor.sys.code.entity.TableInfo;

import java.util.List;


public interface CodeService {

	public List<String> findTableList();
	public List<ColumnInfo> findColumnsByTable(String tableName);
	public TableInfo getTableInfoByName(String tableName);
}
