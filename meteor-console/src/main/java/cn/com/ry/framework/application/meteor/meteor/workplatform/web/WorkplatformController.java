package cn.com.ry.framework.application.meteor.meteor.workplatform.web;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecFunction;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/meteor/workplatform")
public class WorkplatformController extends SpringControllerSupport {

    @SecPrivilege(title = "工作台")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }

    @RequestMapping(value = "/jad")
    @SecFunction(title="反编译",code="jad")
    public String jad(Model model,
                       @QueryParameter XJJParameter query,
                       @ModelAttribute("page") Pagination page
    ) {
        return getViewPath("jad");
    }
}
