<#include "/templates/xjj-index.ftl">
<@content>
    <@query>
        <@button type="info" onclick="sendMachineCmd()">开启Agent</@button>(先开启agent)
        <@querygroup title='选择应用'>
            <input type="text" style="width:80px;" id="appId" value="1" class="form-control input-sm"
                   check-type="required">
        </@querygroup>
        <@button type="info" onclick="selectAppId()">确定应用</@button> (agent启动后再选择)

        <@button type="info" onclick="shutDownAgent()">关闭Agent</@button> (进入agent再shutdown)
    </@query>
    <div class="row">
        <div class="container-fluid px-0">
            <div class="col px-0" id="machine-console-card">
                <div id="machine-console"></div>
            </div>
        </div>
    </div>
</@content>

<script type="text/javascript">
    var machine_ws;

    setTimeout(function () {
        initMachineConsole();
    }, 1000);

    function initMachineConsole() {
        var machineId = '${machine.id}';

        if (machineId == '') {
            XJJ.msger('机器ID不能为空');
            return;
        }
        if (machine_ws != null) {
            disconnect(null, machine_ws);
            XJJ.msger('已经连接！');
        }
        //获取websocket连接
        var path = 'ws://${host}/websocketmeteor/machine/' + encodeURIComponent(machineId);

        machine_ws = initWebsocketConsole("machine-console", path, "meteor_machine", true, modifyServerStatus);
    }

    function sendMachineCmd() {
        var arthasAgentId = '${machine.arthasAgentId}';
        var cmd = "";
        if (arthasAgentId.trim() == "") {
            cmd = "jps | grep -wq 'arthas-boot.jar' && echo 'arthas已经开启'|| java -jar /tmp/meteor/arthas-boot.jar --target-ip 0.0.0.0";
        } else {
            cmd = "jps | grep -wq 'arthas-boot.jar' && echo 'arthas已经开启'|| java -jar /tmp/meteor/arthas-boot.jar --tunnel-server 'ws://${machine.arthasIp}:${machine.arthasPort}/ws' --agent-id ${machine.arthasAgentId}";
        }
        machine_ws.send(cmd + "\r");
    }

    //shutdown Agent
    function shutDownAgent() {
        var cmd = "shutdown";
        machine_ws.send(cmd + "\r");
    }

    //判断arthas是否开启，修改状态
    function modifyServerStatus(data) {
        if (null != data && data.indexOf("[arthas@") >= 0) {
            var id = '${machine.id}';
            var url = "${base}/meteor/machineinfo/modifyServerStatus/" + id + "/2";
            if (data.indexOf("going to shut") >= 0) {
                url = "${base}/meteor/machineinfo/modifyServerStatus/" + id + "/4";
            }
            $.ajax({
                type: "GET",
                async: false,
                dataType: "JSON",
                contentType: "application/json;charset=UTF-8",
                url: url,
                success: function (result) {
                    if (result.message != '' && result.type == 'success') {
                        XJJ.msgok(result.message);
                        XJJ.query({id: 'meteor_machineinfo'});
                    } else {
                        XJJ.msger(result.message);
                    }
                },
                error: function (e) {
                    XJJ.msger(e.responseText);
                    console.log(e.status);
                    console.log(e.responseText);
                }

            });
        }
    }

    function selectAppId() {
        var appId = $("#appId").val();
        machine_ws.send(appId + "\r");
    }

    //清除machine_ws
    var meteorInterval = window.setInterval(function () {
        if ($("#machine-console").html() == undefined) {
            disconnect(null, machine_ws);
            clearInterval(meteorInterval);
        }
    }, 5000);

</script>

